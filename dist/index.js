"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_token_1 = __importDefault(require("api-token"));
class default_1 {
    // ALL API V1 Outputs should be JSON.
    constructor(app, accountSystem, filter) {
        this.version = "/api/v1";
        this.accountSystem = accountSystem;
        this.app = app;
        this.apiToken = api_token_1.default;
        this.filter = filter;
        /* set expiration time to 2 minutes */
        this.apiToken.setExpirationTime(15);
    }
    addOwnEndpoints() {
        // Ensure token validator is on for all endpoints.
        this.validToken();
        // Add the actual endpoints.
        this.authenticate();
        this.addUser();
        this.authUser();
        // Ensure all API routes are
        this.notEndpoint();
    }
    validToken() {
        let { app, version, apiToken, } = this;
        app.all((version + '/*'), (req, res, next) => {
            if (req.url === (version + '/authenticate') || req.url === (version + "/parseToken")) {
                /* token is not needed when authenticating or to retreve a tokens data. */
                next();
            }
            else if (req.body) {
                if (apiToken.isTokenValid(req.body.token)) {
                    /* if token is valid continue */
                    next();
                }
                else {
                    res.type('json');
                    /* if token is not valid send unauthorized http statuscode to client */
                    res.status(401).json({ error: "Invalid Token" });
                }
            }
            else {
                res.type('json');
                /* if token is not valid send unauthorized http statuscode to client */
                res.status(401).json({ error: "Invalid Token" });
            }
        });
    }
    authenticate() {
        let { app, version, apiToken, accountSystem, } = this;
        // This is how you get a token.
        app.post((version + '/authenticate'), async (req, res) => {
            res.type('json');
            var authenticated = false;
            let user = null;
            if (req.body) {
                let type = req.body.type;
                let username = req.body.username;
                let password = req.body.password;
                if (type == "createUser") {
                    // If trying to create a user.
                    // No password is required, just create one.
                    user = apiToken.addUser("newUserOnly");
                    authenticated = true;
                }
                else if (type == "login") {
                    // if trying to authenticate.
                    // Try authenticateing.
                    authenticated = await accountSystem.authUser(username, password);
                    if (authenticated) {
                        // Then generate a login token.
                        user = apiToken.addUser(username);
                    }
                }
                if (!authenticated || user == null) {
                    // Catch all for failure to authenticate.
                    res.status(401).json({ error: "Could Not Authenticate" });
                }
                else {
                    // Authenticated users get token.
                    res.status(200).json({ token: user.token });
                }
            }
            else {
                res.status(400).json({ error: "Malformed Request" });
            }
        });
    }
    addUser() {
        let { app, version, apiToken, accountSystem, } = this;
        //This is how to create a user.
        app.post((version + "/addUser"), async (req, resp) => {
            resp.type('json');
            const body = req.body;
            let authUser = apiToken.findUserByToken(body.token);
            // Must be a new user token. (And tokens are only valid for one go.)
            if (authUser.username != "newUserOnly") {
                resp.status(401).json({ error: "Unauthorized" });
            }
            else if (body.username && body.password) {
                let { message: username, flaged } = this.filter(body.username);
                if (flaged) {
                    resp.status(422).json({ error: "Did Not Pass Filter" });
                }
                else {
                    let userExsists = await accountSystem.getUserSafe(username);
                    if (userExsists == false) {
                        let { success: added, error } = await accountSystem.addUser(username, body.password);
                        if (added) {
                            resp.status(201).json({ created: username });
                        }
                        else {
                            resp.status(500).json({ error: (error + "") });
                            console.log(error);
                        }
                    }
                    else {
                        resp.status(423).json({ error: "User Already Exsists" });
                    }
                }
            }
            else {
                resp.status(400).json({ error: "Malformed Request" });
            }
        });
    }
    authUser() {
        let { app, version, apiToken, accountSystem, } = this;
        //This is how to create a user.
        app.post((version + "/authUser"), async (req, resp) => {
            resp.type('json');
            const body = req.body;
            let authUsername = apiToken.findUserByToken(body.token);
            let authUser = await accountSystem.getUserSafe(authUsername.username);
            if (authUser !== false) {
                if (!accountSystem.isAdmin(authUser)) {
                    resp.status(401).json({ error: "Unauthorized" });
                }
                else if (body.username) {
                    let { message: username, flaged } = this.filter(body.username);
                    if (flaged) {
                        resp.status(422).json({ error: "Did Not Pass Filter" });
                    }
                    else {
                        let { success: verified, error } = await accountSystem.addUser(username, body.password);
                        if (verified) {
                            resp.status(200).json({ verified: username });
                        }
                        else {
                            resp.status(500).json({ error: (error + "") });
                            console.log(error);
                        }
                    }
                }
                else {
                    resp.status(400).json({ error: "Malformed Request" });
                }
            }
        });
    }
    parseToken() {
        let { app, version, apiToken, accountSystem, } = this;
        //This is how to create a user.
        app.post((version + "/parseToken"), async (req, resp) => {
            resp.type('json');
            const body = req.body;
            let authUser = apiToken.findUserByToken(body.token);
            let userData = await accountSystem.getUserSafe(authUser.username);
            if (userData == false) {
                resp.status(404).json({ error: "user not found" });
            }
            else {
                resp.status(203).json({ user: authUser.username, data: userData });
            }
        });
    }
    notEndpoint() {
        let { app, version, } = this;
        app.all((version + '/*'), function (_req, res) {
            res.type('json');
            res.status(400).json({ error: "Invalid Endpoint or Method" });
        });
    }
}
exports.default = default_1;
