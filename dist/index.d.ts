import ProfanityFilter from "profanity-filter-interface";
import express from "express";
import AccountSystem from "account-database-interface";
export default class {
    private version;
    private accountSystem;
    private app;
    private apiToken;
    private filter;
    constructor(app: express.Application, accountSystem: AccountSystem, filter: ProfanityFilter["filter"]);
    addOwnEndpoints(): void;
    validToken(): void;
    authenticate(): void;
    addUser(): void;
    authUser(): void;
    parseToken(): void;
    notEndpoint(): void;
}
